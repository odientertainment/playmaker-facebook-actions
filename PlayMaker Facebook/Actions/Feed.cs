﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("ODI Facebook")]
	[Tooltip("Post no facebook")]
	public class Feed : FsmStateAction {

		[RequiredField]
		[UIHint(UIHint.Variable)]
		public FsmInt playerRecord;

		public string Link = "";
		public string LinkName = "";
		public string LinkCaption = "";
		public string LinkDescription = "";
		public string Picture = "";

		public override void OnEnter()
		{
			FB.Feed(  
			        link: Link,
			        linkName: LinkName,
			        linkCaption: LinkCaption,
			        linkDescription: LinkDescription.Replace("{0}", playerRecord.ToString()),
			        picture: Picture
			        );

			Finish();			      
		}
	}
}