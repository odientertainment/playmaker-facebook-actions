﻿using UnityEngine;
using System.Collections;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("ODI Facebook")]
	[Tooltip("Efetua o login no facebook.")]
	public class SingIn : FsmStateAction {

		[Tooltip("Success Event")]
		public FsmEvent OnSuccess;
		
		[Tooltip("Error Event")]
		public FsmEvent OnError;

		public override void OnEnter()
		{
			if(FB.IsLoggedIn) {
				//caso ja esteja logado
				Success();
				Finish();
			}else {
				//caso nao esteja, efetua o login
				FB.Login("email,publish_actions", LoginCallback);
			}
		}

		public void LoginCallback(FBResult result)
		{
			if (result.Error != null || !FB.IsLoggedIn){
				Error();
			}else{
				Success();
			}
			Finish();
		}

		public void Error()
		{
			Fsm.Event(OnError);
			PlayerPrefs.SetInt("FBIsLogged", 0);
		}

		public void Success()
		{
			Fsm.Event(OnSuccess);
			PlayerPrefs.SetInt("FBIsLogged", 1);
		}
	}
}
