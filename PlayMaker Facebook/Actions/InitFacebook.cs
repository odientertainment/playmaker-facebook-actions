﻿using UnityEngine;
using System.Collections;
using System;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("ODI Facebook")]
	[Tooltip("Inicializa a API do facebook. Deve ser chamada antes de usar qualquer action do facebook.")]
	public class InitFacebook : FsmStateAction
	{		
		public override void OnEnter()
		{
			if(FB.AppId == null){
				FB.Init(OnInitComplete);
			}else{
				Finish();
			}
		}
		
		private void OnInitComplete()
		{
			Finish();
		}
	}
}